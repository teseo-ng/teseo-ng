import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeseoAuthGuard } from '@teseo/service/teseo-auth.guard';
import { WbLayoutComponent } from './layouts/wb-layout/wb-layout.component';

const routes: Routes = [

  // Workbench - / 
  // ----------------------------------------------------------------------------------------------

  {
    path: '', canActivate: [TeseoAuthGuard], component: WbLayoutComponent, children: [
      { path: '', loadChildren: () => import('./detritus-wb/detritus-wb.module').then(m => m.DetritusWbModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
