import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetritusWbMainPageComponent } from './detritus-wb-main-page.component';

describe('DetritusWbMainPageComponent', () => {
  let component: DetritusWbMainPageComponent;
  let fixture: ComponentFixture<DetritusWbMainPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetritusWbMainPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetritusWbMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
