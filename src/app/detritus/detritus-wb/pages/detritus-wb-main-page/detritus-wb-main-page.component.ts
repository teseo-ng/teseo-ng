import { Component, OnInit } from '@angular/core';
import { TeseoAuthService } from '@teseo/service/teseo-auth.service';

@Component({
  selector: 'app-detritus-wb-main-page',
  templateUrl: './detritus-wb-main-page.component.html',
  styleUrls: ['./detritus-wb-main-page.component.css']
})
export class DetritusWbMainPageComponent implements OnInit {

  loggedUser: any;

  constructor(
    private teseoAuthService: TeseoAuthService
  ) { }

  ngOnInit() {
    this.loggedUser = this.teseoAuthService.getCurrentUser().displayName;
  }

}
