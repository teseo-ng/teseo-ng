import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetritusWbMainPageComponent } from './pages/detritus-wb-main-page/detritus-wb-main-page.component';

const routes: Routes = [
  { path: '', component: DetritusWbMainPageComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DetritusWbRoutingModule { }
