import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetritusWbMainPageComponent } from './pages/detritus-wb-main-page/detritus-wb-main-page.component';
import { DetritusWbRoutingModule } from './detritus-wb-routing.module';

@NgModule({
  declarations: [
    DetritusWbMainPageComponent
  ],
  imports: [
    CommonModule,
    DetritusWbRoutingModule
  ]
})
export class DetritusWbModule { }
