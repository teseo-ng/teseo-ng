import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WbLayoutComponent } from './wb-layout.component';

describe('WbLayoutComponent', () => {
  let component: WbLayoutComponent;
  let fixture: ComponentFixture<WbLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WbLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WbLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
