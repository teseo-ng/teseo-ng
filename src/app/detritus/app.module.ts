import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { LayoutsModule } from '../teseo/layouts/layouts.module';
import { WbLayoutComponent } from './layouts/wb-layout/wb-layout.component';

@NgModule({
  declarations: [
    WbLayoutComponent
  ],
  imports: [
    CommonModule,
    LayoutsModule,
    AppRoutingModule
  ],
  exports: [
  ]
})
export class AppModule { }
