import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PrivateLayoutComponent } from './private-layout/private-layout.component';
import { TeseoCommonModule } from '../modules/teseo-common/teseo-common.module';
import { PublicLayoutComponent } from './public-layout/public-layout.component';

@NgModule({
  declarations: [
    PrivateLayoutComponent,
    PublicLayoutComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    TeseoCommonModule
  ],
  exports: [
    PrivateLayoutComponent
  ]
})
export class LayoutsModule { }
