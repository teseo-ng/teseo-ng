import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TeseoAuthService } from './teseo-auth.service';

@Injectable({
  providedIn: 'root'
})
export class TeseoAuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private teseoAuthService: TeseoAuthService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (!this.teseoAuthService.getCurrentUser()) {
      this.router.navigate(['/public/login'], { queryParams: { returnUrl: state.url } });
      return false;
    }

    return true;
  }

}
