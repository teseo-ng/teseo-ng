import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class TeseoAuthService {

  constructor(
    public fireAuth: AngularFireAuth
  ) { }

  /**
   * Get current user.
   * 
   * @returns {firebase.User} Firebase user
   */
  public getCurrentUser(): firebase.User {
    return this.fireAuth.auth.currentUser;
  }

}
