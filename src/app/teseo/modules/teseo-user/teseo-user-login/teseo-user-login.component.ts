import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { timeout } from 'q';

@Component({
  selector: 'app-teseo-user-login',
  templateUrl: './teseo-user-login.component.html',
  styleUrls: ['./teseo-user-login.component.css']
})
export class TeseoUserLoginComponent implements OnInit {

  private STATUS_VIEW = { 'AUTHENTICATING': 'AUTHENTICATING', 'LOGIN': 'LOGIN', 'ERROR': 'ERROR', };

  status = this.STATUS_VIEW.LOGIN;
  message: string;
  submitted = false;
  errorTimeout: any;

  userForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(
    public router: Router,
    public fireAuth: AngularFireAuth
  ) { }

  ngOnInit() {
  }

  /**
   * On submit login form.
   */
  onSubmit() {
    this.submitted = true;
    if (this.userForm.invalid) { return; }

    this.changeStatusView(this.STATUS_VIEW.AUTHENTICATING);

    this.fireAuth.auth.signInWithEmailAndPassword(this.userForm.value.email, this.userForm.value.password).then(result => {
      this.router.navigate(['/']); // , { queryParams: { returnUrl: state.url } });
    }).catch(() => {
      this.changeStatusView(this.STATUS_VIEW.ERROR, 'Invalid user or password.');
    });
  }

  /**
   * Change status view.
   * 
   * @param status New status view
   * @param message 
   */
  private changeStatusView(status: string, message?: string) {
    this.status = status;
    this.message = message;

    if (this.status === this.STATUS_VIEW.ERROR) {
      clearTimeout(this.errorTimeout);
      this.errorTimeout = setTimeout(() => { this.status = this.STATUS_VIEW.LOGIN; }, 2000);
    }
  }

  get email() { return this.userForm.get('email'); }
  get password() { return this.userForm.get('password'); }
}
