import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TeseoUserCreateComponent } from './teseo-user-create/teseo-user-create.component';
import { TeseoUserLayoutComponent } from './teseo-user-layout/teseo-user-layout.component';
import { TeseoUserLoginComponent } from './teseo-user-login/teseo-user-login.component';
import { TeseoUserRoutingModule } from './teseo-user-routing.module';
import { TeseoUserResetPasswordComponent } from './teseo-user-reset-password/teseo-user-reset-password.component';

@NgModule({
  declarations: [
    TeseoUserLoginComponent,
    TeseoUserCreateComponent,

    TeseoUserLayoutComponent,

    TeseoUserResetPasswordComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,

    TeseoUserRoutingModule
  ],
  exports: [
    TeseoUserLoginComponent,
    TeseoUserCreateComponent
  ]
})
export class TeseoUserModule { }
