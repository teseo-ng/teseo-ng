import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-teseo-user-create',
  templateUrl: './teseo-user-create.component.html',
  styleUrls: ['./teseo-user-create.component.css']
})
export class TeseoUserCreateComponent implements OnInit {

  submitted = false;

  userForm = new FormGroup({
    // username: new FormControl('', [Validators.required, Validators.minLength(4)]),
    // fullname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(4)]),
  });

  constructor(
    public fireAuth: AngularFireAuth
  ) { }

  ngOnInit() {
  }

  get f() { return this.userForm.controls; }

  onSubmit() {
    this.submitted = true;
    this.fireAuth.auth.createUserWithEmailAndPassword(this.userForm.value.email, this.userForm.value.password).then(result => {
    }).catch(err => {
      console.log(err);
    });

    if (!this.userForm.invalid) {
      // this.authService.createUser(this.userForm.value.email, this.userForm.value.password);
      console.log('valid form');
    }

    console.warn(this.userForm.value);
  }

}
