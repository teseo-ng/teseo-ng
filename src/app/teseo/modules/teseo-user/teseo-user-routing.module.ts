import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeseoUserCreateComponent } from './teseo-user-create/teseo-user-create.component';
import { TeseoUserLayoutComponent } from './teseo-user-layout/teseo-user-layout.component';
import { TeseoUserLoginComponent } from './teseo-user-login/teseo-user-login.component';
import { TeseoUserResetPasswordComponent } from './teseo-user-reset-password/teseo-user-reset-password.component';

const routes: Routes = [
  {
    path: '', component: TeseoUserLayoutComponent, children: [
      { path: 'login', component: TeseoUserLoginComponent },
      { path: 'user/create', component: TeseoUserCreateComponent },
      { path: 'user/password/reset', component: TeseoUserResetPasswordComponent },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TeseoUserRoutingModule { }
