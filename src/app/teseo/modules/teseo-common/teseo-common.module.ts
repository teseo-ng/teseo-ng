import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeseoMenuComponent } from './teseo-menu/teseo-menu.component';

@NgModule({
  declarations: [
    TeseoMenuComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TeseoMenuComponent
  ]
})
export class TeseoCommonModule { }
