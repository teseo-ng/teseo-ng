import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicLayoutComponent } from '@teseo/layout/public-layout.component';

// Layouts

const routes: Routes = [

  // PublicLayout - /public/
  // ----------------------------------------------------------------------------------------------

  {
    path: '', component: PublicLayoutComponent, children: [
      { path: 'public', loadChildren: () => import('./modules/teseo-user/teseo-user.module').then(m => m.TeseoUserModule) }
    ]
  }

  // PrivateLayout - /private/
  // ----------------------------------------------------------------------------------------------

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TeseoRoutingModule { }
