import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LayoutsModule } from './layouts/layouts.module';
import { TeseoRoutingModule } from './teseo-routing.module';
import { TeseoComponent } from './teseo.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../../environments/environment';
import { AppModule } from '../detritus/app.module';

@NgModule({
  declarations: [
    TeseoComponent
  ],
  imports: [
    BrowserModule,

    AppModule,
    TeseoRoutingModule,

    // Layouts
    LayoutsModule,

    // Firebase
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth feature  
  ],
  providers: [],
  bootstrap: [TeseoComponent]
})
export class TeseoModule { }
